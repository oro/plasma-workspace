# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2023 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-09 02:16+0000\n"
"PO-Revision-Date: 2023-10-29 00:38+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#: abstractnotificationsmodel.cpp:315
#, kde-format
msgctxt "@info %1 notification body %2 application name"
msgid "%1 from %2"
msgstr ""

#: job_p.cpp:183
#, kde-format
msgctxt "Copying n of m files to locaton"
msgid "%2 of %1 file to %3"
msgid_plural "%2 of %1 files to %3"
msgstr[0] ""
msgstr[1] ""

#: job_p.cpp:186 job_p.cpp:216 job_p.cpp:232
#, kde-format
msgctxt "Copying n files to location"
msgid "%1 file to %2"
msgid_plural "%1 files to %2"
msgstr[0] ""
msgstr[1] ""

#: job_p.cpp:193
#, kde-format
msgctxt "Copying n of m files"
msgid "%2 of %1 file"
msgid_plural "%2 of %1 files"
msgstr[0] "%2 de %1 ficheru"
msgstr[1] "%2 de %1 ficheros"

#: job_p.cpp:196 job_p.cpp:226 job_p.cpp:236
#, kde-format
msgctxt "Copying n files"
msgid "%1 file"
msgid_plural "%1 files"
msgstr[0] "%1 ficheru"
msgstr[1] "%1 ficheros"

#: job_p.cpp:201
#, kde-format
msgctxt "Copying n of m items"
msgid "%2 of %1 item"
msgid_plural "%2 of %1 items"
msgstr[0] "%2 de %1 elementu"
msgstr[1] "%2 de %1 elementos"

#: job_p.cpp:204 job_p.cpp:224
#, kde-format
msgctxt "Copying n items"
msgid "%1 item"
msgid_plural "%1 items"
msgstr[0] "%1 elementu"
msgstr[1] "%1 elementos"

#: job_p.cpp:211
#, kde-format
msgctxt "Copying file to location"
msgid "%1 to %2"
msgstr ""

#: job_p.cpp:214
#, kde-format
msgctxt "Copying n items to location"
msgid "%1 file to %2"
msgid_plural "%1 items to %2"
msgstr[0] ""
msgstr[1] ""

#: job_p.cpp:234
#, kde-format
msgctxt "Copying unknown amount of files to location"
msgid "to %1"
msgstr ""

#: jobsmodel.cpp:105
#, kde-format
msgctxt "@info %1 notification body %2 job name"
msgid "%1 from %2"
msgstr ""

#: jobsmodel_p.cpp:477
#, kde-format
msgid "Application closed unexpectedly."
msgstr ""
