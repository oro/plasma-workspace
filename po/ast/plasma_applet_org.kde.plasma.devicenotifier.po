# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2023 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-28 01:41+0000\n"
"PO-Revision-Date: 2023-11-07 21:27+0100\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#: package/contents/ui/DeviceItem.qml:185
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr ""

#: package/contents/ui/DeviceItem.qml:189
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr "Accediendo…"

#: package/contents/ui/DeviceItem.qml:192
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr "Estrayendo…"

#: package/contents/ui/DeviceItem.qml:195
#, kde-format
msgid "Don't unplug yet! Files are still being transferred..."
msgstr "¡Nun lu estrayas entá! Hai ficheros en tresferencia…"

#: package/contents/ui/DeviceItem.qml:226
#, kde-format
msgid "Open in File Manager"
msgstr "Abrir nel xestor de ficheros"

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Mount and Open"
msgstr "Montar ya abrir"

#: package/contents/ui/DeviceItem.qml:231
#, kde-format
msgid "Eject"
msgstr "Espulsar"

#: package/contents/ui/DeviceItem.qml:233
#, kde-format
msgid "Safely remove"
msgstr "Estrayer con seguranza"

#: package/contents/ui/DeviceItem.qml:275
#, kde-format
msgid "Mount"
msgstr "Montar"

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:252
#, kde-format
msgid "Remove All"
msgstr "Estrayer too"

#: package/contents/ui/FullRepresentation.qml:44
#, kde-format
msgid "Click to safely remove all devices"
msgstr "Calca pa estrayer con seguranza tolos preseos"

#: package/contents/ui/FullRepresentation.qml:185
#, kde-format
msgid "No removable devices attached"
msgstr "Nun s'espató nengun preséu estrayible"

#: package/contents/ui/FullRepresentation.qml:185
#, kde-format
msgid "No disks available"
msgstr "Nun hai nengún discu disponible"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "Most Recent Device"
msgstr "El preséu más recién"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "No Devices Available"
msgstr "Nun hai nengún preséu disponible"

#: package/contents/ui/main.qml:234
#, kde-format
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "Configurar los preseos estrayibles…"

#: package/contents/ui/main.qml:260
#, kde-format
msgid "Removable Devices"
msgstr "Preseos estrayibles"

#: package/contents/ui/main.qml:275
#, kde-format
msgid "Non Removable Devices"
msgstr "Preseos non estrayibles"

#: package/contents/ui/main.qml:290
#, kde-format
msgid "All Devices"
msgstr "Tolos preseos"

#: package/contents/ui/main.qml:307
#, kde-format
msgid "Show popup when new device is plugged in"
msgstr "Amosar el ventanu al espatar un preséu nuevu"
