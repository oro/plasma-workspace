# translation of plasma_wallpaper_org.kde.image.pot to esperanto
# Copyright (C) 2010 Free Software Foundation, Inc.
# This file is distributed under the same license as the plasma package.
# Axel Rousseau <axel@esperanto-jeunes.org>, 2010.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-26 01:47+0000\n"
"PO-Revision-Date: 2023-09-15 07:39+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Axel Rousseau,Oliver Kellogg"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "axel@esperanto-jeunes.org,okellogg@users.sourceforge.net"

#: imagepackage/contents/ui/AddFileDialog.qml:57
#, kde-format
msgctxt "@title:window"
msgid "Open Image"
msgstr "Malfermi Bildon"

#: imagepackage/contents/ui/AddFileDialog.qml:69
#, kde-format
msgctxt "@title:window"
msgid "Directory with the wallpaper to show slides from"
msgstr "Dosierujo kun ekranfonbildo el kiu montri lumbildojn"

#: imagepackage/contents/ui/config.qml:100
#, kde-format
msgid "Positioning:"
msgstr "Lokigo:"

#: imagepackage/contents/ui/config.qml:103
#, kde-format
msgid "Scaled and Cropped"
msgstr "Skalite kaj Stucite"

#: imagepackage/contents/ui/config.qml:107
#, kde-format
msgid "Scaled"
msgstr "Skalite"

#: imagepackage/contents/ui/config.qml:111
#, kde-format
msgid "Scaled, Keep Proportions"
msgstr "Skalite"

#: imagepackage/contents/ui/config.qml:115
#, kde-format
msgid "Centered"
msgstr "Centre"

#: imagepackage/contents/ui/config.qml:119
#, kde-format
msgid "Tiled"
msgstr "Kahele"

#: imagepackage/contents/ui/config.qml:147
#, kde-format
msgid "Background:"
msgstr "Fono:"

#: imagepackage/contents/ui/config.qml:148
#, kde-format
msgid "Blur"
msgstr "Malklara"

#: imagepackage/contents/ui/config.qml:157
#, kde-format
msgid "Solid color"
msgstr "Solida koloro"

#: imagepackage/contents/ui/config.qml:167
#, kde-format
msgid "Select Background Color"
msgstr "Elekti Fonan Koloron"

#: imagepackage/contents/ui/main.qml:34
#, kde-format
msgid "Open Wallpaper Image"
msgstr "Malfermi Ekranfonan Bildon"

#: imagepackage/contents/ui/main.qml:40
#, kde-format
msgid "Next Wallpaper Image"
msgstr "Sekva Ekranfona Bildo"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:70
#, kde-format
msgid "Images"
msgstr "Bildoj"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:74
#, kde-format
msgctxt "@action:button the thing being added is an image file"
msgid "Add…"
msgstr "Aldoni…"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:80
#, kde-format
msgctxt "@action:button the new things being gotten are wallpapers"
msgid "Get New…"
msgstr "Ricevi Novajn…"

#: imagepackage/contents/ui/WallpaperDelegate.qml:31
#, kde-format
msgid "Open Containing Folder"
msgstr "Malfermi Entenantan Dosierujon"

#: imagepackage/contents/ui/WallpaperDelegate.qml:37
#, kde-format
msgid "Restore wallpaper"
msgstr "Restarigi ekranfonbildon"

#: imagepackage/contents/ui/WallpaperDelegate.qml:42
#, kde-format
msgid "Remove Wallpaper"
msgstr "Forigi Ekranfonbildon"

#: plasma-apply-wallpaperimage.cpp:29
#, kde-format
msgid ""
"This tool allows you to set an image as the wallpaper for the Plasma session."
msgstr "Tiu ilo permesas meti bildon kiel ekranfono de la Plasma-seanco."

#: plasma-apply-wallpaperimage.cpp:31
#, kde-format
msgid ""
"An image file or an installed wallpaper kpackage that you wish to set as the "
"wallpaper for your Plasma session"
msgstr ""
"Bilddosiero aŭ instalita ekranfonbilda kpackage kiun vi deziras meti kiel "
"ekranfonbildo de via Plasma-seanco"

#: plasma-apply-wallpaperimage.cpp:45
#, kde-format
msgid ""
"There is a stray single quote in the filename of this wallpaper (') - please "
"contact the author of the wallpaper to fix this, or rename the file "
"yourself: %1"
msgstr ""
"Estas vaga unuopa citilo en la dosiernomo de ĉi tiu tapeto (') - bonvolu "
"kontaktu la aŭtoron de la tapeto por ripari ĝin, aŭ alinomu la dosieron vi "
"mem: %1"

#: plasma-apply-wallpaperimage.cpp:85
#, kde-format
msgid "An error occurred while attempting to set the Plasma wallpaper:\n"
msgstr "Eraro okazis dum provo meti la Plasma-ekranfonbildon:\n"

#: plasma-apply-wallpaperimage.cpp:89
#, kde-format
msgid ""
"Successfully set the wallpaper for all desktops to the KPackage based %1"
msgstr ""
"Sukcese metis la ekranfonbildon por ĉiuj labortabloj de la KPackage-bazita %1"

#: plasma-apply-wallpaperimage.cpp:91
#, kde-format
msgid "Successfully set the wallpaper for all desktops to the image %1"
msgstr "Sukcese metis la ekranfonbildon por ĉiuj labortabloj al la bildo %1"

#: plasma-apply-wallpaperimage.cpp:97
#, kde-format
msgid ""
"The file passed to be set as wallpaper does not exist, or we cannot identify "
"it as a wallpaper: %1"
msgstr ""
"La dosiero metenda kiel ekranfonbildo ne ekzistas aŭ ni ne povas identigi "
"ĝin kiel ekranfonbildo: %1"

#. i18n people, this isn't a "word puzzle". there is a specific string format for QFileDialog::setNameFilters
#: plugin/imagebackend.cpp:336
#, kde-format
msgid "Image Files"
msgstr "Bilddosieroj"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:37
#, kde-format
msgid "Order:"
msgstr "Sinsekvo:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:44
#, kde-format
msgid "Random"
msgstr "Hazarde"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:48
#, kde-format
msgid "A to Z"
msgstr "A ĝis Z"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:52
#, kde-format
msgid "Z to A"
msgstr "Z ĝis A"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:56
#, kde-format
msgid "Date modified (newest first)"
msgstr "Dato modifita (plej nova unue)"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:60
#, kde-format
msgid "Date modified (oldest first)"
msgstr "Dato modifita (plej malnova unue)"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:85
#, kde-format
msgid "Group by folders"
msgstr "Grupigi laŭ dosierujoj"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:97
#, kde-format
msgid "Change every:"
msgstr "Ŝanĝi ĉiujn:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:107
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 horon"
msgstr[1] "%1 horojn"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:127
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuton"
msgstr[1] "%1 minutojn"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:147
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekundon"
msgstr[1] "%1 sekundojn"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:178
#, kde-format
msgid "Folders"
msgstr "Dosierujoj"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:182
#, kde-format
msgctxt "@action button the thing being added is a folder"
msgid "Add…"
msgstr "Aldoni…"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:220
#, kde-format
msgid "Remove Folder"
msgstr "Forigi Dosierujon"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:231
#, kde-format
msgid "Open Folder…"
msgstr "Malfermi Dosierujon…"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:246
#, kde-format
msgid "There are no wallpaper locations configured"
msgstr "Ne estas agordita iu ekranfonbildejo"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:47
#, kde-format
msgctxt "@action:inmenu"
msgid "Set as Wallpaper"
msgstr "Meti kiel Ekranfonbildon"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:50
#, kde-format
msgctxt "@action:inmenu Set as Desktop Wallpaper"
msgid "Desktop"
msgstr "Labortablo"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:56
#, kde-format
msgctxt "@action:inmenu Set as Lockscreen Wallpaper"
msgid "Lockscreen"
msgstr "Ŝlosekrano"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:62
#, kde-format
msgctxt "@action:inmenu Set as both lockscreen and Desktop Wallpaper"
msgid "Both"
msgstr "Ambaŭ"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:98
#, kde-kuit-format
msgctxt "@info %1 is the dbus error message"
msgid "An error occurred while attempting to set the Plasma wallpaper:<nl/>%1"
msgstr "Eraro okazis dum provo meti la Plasma-ekranfonbildon:<nl/>%1"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:111
#, kde-format
msgid "An error occurred while attempting to open kscreenlockerrc config file."
msgstr "Eraro okazis dum provo malfermi kscreenlockerrc-agorddosieron."
