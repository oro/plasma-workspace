# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Diego Iastrubni <elcuco@kde.org>, 2008, 2013.
# elkana bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-28 01:41+0000\n"
"PO-Revision-Date: 2023-10-04 09:23+0300\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Poedit 3.3.2\n"

#: package/contents/ui/DeviceItem.qml:185
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr "%1 פנויים מתוך %2"

#: package/contents/ui/DeviceItem.qml:189
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr "מתבצעת גישה…"

#: package/contents/ui/DeviceItem.qml:192
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr "מתבצעת הסרה…"

#: package/contents/ui/DeviceItem.qml:195
#, kde-format
msgid "Don't unplug yet! Files are still being transferred..."
msgstr "לא לנתק עוד! עדיין יש קבצים שמועברים…"

#: package/contents/ui/DeviceItem.qml:226
#, kde-format
msgid "Open in File Manager"
msgstr "פתיחת מנהל הקבצים"

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Mount and Open"
msgstr "עיגון ופתיחה"

#: package/contents/ui/DeviceItem.qml:231
#, kde-format
msgid "Eject"
msgstr "שליפה"

#: package/contents/ui/DeviceItem.qml:233
#, kde-format
msgid "Safely remove"
msgstr "הסרה בבטחה"

#: package/contents/ui/DeviceItem.qml:275
#, kde-format
msgid "Mount"
msgstr "עיגון"

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:252
#, kde-format
msgid "Remove All"
msgstr "להסיר הכול"

#: package/contents/ui/FullRepresentation.qml:44
#, kde-format
msgid "Click to safely remove all devices"
msgstr "יש ללחוץ כדאי להסיר בבטחה את כל ההתקנים"

#: package/contents/ui/FullRepresentation.qml:185
#, kde-format
msgid "No removable devices attached"
msgstr "אין התקנים נתיקים מחוברים"

#: package/contents/ui/FullRepresentation.qml:185
#, kde-format
msgid "No disks available"
msgstr "אין כוננים זמינים"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "Most Recent Device"
msgstr "ההתקן העדכני ביותר"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "No Devices Available"
msgstr "אין התקנים זמינים"

#: package/contents/ui/main.qml:234
#, kde-format
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "הגדרת התקנים נתיקים…"

#: package/contents/ui/main.qml:260
#, kde-format
msgid "Removable Devices"
msgstr "התקנים נתיקים"

#: package/contents/ui/main.qml:275
#, kde-format
msgid "Non Removable Devices"
msgstr "התקנים קבועים"

#: package/contents/ui/main.qml:290
#, kde-format
msgid "All Devices"
msgstr "כל ההתקנים"

#: package/contents/ui/main.qml:307
#, kde-format
msgid "Show popup when new device is plugged in"
msgstr "הצגת חלונית קופצת עם חיבור התקן חדש"

#~ msgid "General"
#~ msgstr "כללי"

#~ msgid ""
#~ "It is currently <b>not safe</b> to remove this device: applications may "
#~ "be accessing it. Click the eject button to safely remove this device."
#~ msgstr ""
#~ "כרגע <b>לא בטוח</b> להסיר את ההתקן, מכיוון שקיימים יישומים הניגשים אליו. "
#~ "לחץ על לחצן ההסרה כדי להסיר בבטחה את ההתקן."

#~ msgid "This device is currently accessible."
#~ msgstr "ההתקן הזה זמין כעת."

#~ msgid ""
#~ "It is currently <b>not safe</b> to remove this device: applications may "
#~ "be accessing other volumes on this device. Click the eject button on "
#~ "these other volumes to safely remove this device."
#~ msgstr ""
#~ "כרגע <b>לא בטוח</b> להסיר את ההתקן, מכיוון שקיימים יישומים הניגשים אל "
#~ "חלקים אחרים בהתקן. לחץ על לחצן ההסרה של החלקים האחרים כדי להסיר בבטחה את "
#~ "ההתקן."

#~ msgid "It is currently safe to remove this device."
#~ msgstr "בטוח להסיר ההתקן."

#~ msgid "This device is not currently accessible."
#~ msgstr "ההתקן כרגע לא זמין."

#~ msgid "1 action for this device"
#~ msgid_plural "%1 actions for this device"
#~ msgstr[0] "פעולה אחת עבור התקן זה"
#~ msgstr[1] "%1 פעולות עבור התקן זה"

#~ msgid "Click to access this device from other applications."
#~ msgstr "לחץ כדי לגשת להתקן מיישומים אחרים."

#~ msgid "Click to eject this disc."
#~ msgstr "לחץ על מנת להוציא את התקליטור."

#~ msgid "Click to safely remove this device."
#~ msgstr "לחץ כדי להסיר ההתקן בבטחה."
