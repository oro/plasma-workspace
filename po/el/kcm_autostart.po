# translation of kcm_autostart.po to Greek
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Spiros Georgaras <sngeorgaras@otenet.gr>, 2008.
# Toussis Manolis <manolis@koppermind.homelinux.org>, 2008.
# Dimitrios Glentadakis <dglent@gmail.com>, 2011.
# Stelios <sstavra@gmail.com>, 2012.
# Dimitris Kardarakos <dimkard@gmail.com>, 2015, 2017.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-27 01:41+0000\n"
"PO-Revision-Date: 2017-06-12 16:30+0200\n"
"Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: autostartmodel.cpp:374
#, fuzzy, kde-format
#| msgid "\"%1\" is not an absolute path."
msgid "\"%1\" is not an absolute url."
msgstr "Το \"%1\" δεν είναι η πλήρης διαδρομή."

#: autostartmodel.cpp:377
#, kde-format
msgid "\"%1\" does not exist."
msgstr "Το \"%1\" δεν υπάρχει."

#: autostartmodel.cpp:380
#, kde-format
msgid "\"%1\" is not a file."
msgstr "Το \"%1\" δεν είναι αρχείο."

#: autostartmodel.cpp:383
#, kde-format
msgid "\"%1\" is not readable."
msgstr "Το \"%1\" δεν είναι αναγνώσιμο."

#: ui/entry.qml:52
#, fuzzy, kde-format
#| msgid "Name"
msgctxt ""
"@label The name of a Systemd unit for an app or script that will autostart"
msgid "Name:"
msgstr "Όνομα"

#: ui/entry.qml:57
#, fuzzy, kde-format
#| msgid "Status"
msgctxt ""
"@label The current status (e.g. active or inactive) of a Systemd unit for an "
"app or script that will autostart"
msgid "Status:"
msgstr "Κατάσταση"

#: ui/entry.qml:62
#, kde-format
msgctxt ""
"@label A date and time follows this text, making a sentence like 'Last "
"activated on: August 7th 11 PM 2023'"
msgid "Last activated on:"
msgstr ""

#: ui/entry.qml:71
#, kde-format
msgctxt "@label Stop the Systemd unit for a running process"
msgid "Stop"
msgstr ""

#: ui/entry.qml:71
#, fuzzy, kde-format
#| msgid "Startup"
msgctxt "@label Start the Systemd unit for a currently inactive process"
msgid "Start"
msgstr "Έναρξη"

#: ui/entry.qml:105
#, kde-format
msgid "Unable to load logs. Try refreshing."
msgstr ""

#: ui/entry.qml:109
#, kde-format
msgctxt "@action:button Refresh entry logs when it failed to load"
msgid "Refresh"
msgstr ""

#: ui/main.qml:33
#, kde-format
msgid "Make Executable"
msgstr ""

#: ui/main.qml:53
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: ui/main.qml:66
#, kde-format
msgctxt "@action:button"
msgid "Add…"
msgstr ""

#: ui/main.qml:69
#, kde-format
msgctxt "@action:button"
msgid "Add Application…"
msgstr ""

#: ui/main.qml:74
#, fuzzy, kde-format
#| msgid "Add Script..."
msgctxt "@action:button"
msgid "Add Login Script…"
msgstr "Προσθήκη σεναρίου..."

#: ui/main.qml:79
#, fuzzy, kde-format
#| msgid "Add Script..."
msgctxt "@action:button"
msgid "Add Logout Script…"
msgstr "Προσθήκη σεναρίου..."

#: ui/main.qml:119
#, kde-format
msgid ""
"%1 has not been autostarted yet. Details will be available after the system "
"is restarted."
msgstr ""

#: ui/main.qml:143
#, kde-format
msgctxt ""
"@label Entry hasn't been autostarted because system hasn't been restarted"
msgid "Not autostarted yet"
msgstr ""

#: ui/main.qml:148 unit.cpp:30
#, kde-format
msgctxt "@label Entry has failed (exited with an error)"
msgid "Failed"
msgstr ""

#: ui/main.qml:154
#, fuzzy, kde-format
#| msgid "&Properties..."
msgctxt "@action:button"
msgid "See properties"
msgstr "&Ιδιότητες..."

#: ui/main.qml:160
#, fuzzy, kde-format
#| msgid "&Remove"
msgctxt "@action:button"
msgid "Remove entry"
msgstr "Α&φαίρεση"

#: ui/main.qml:170
#, kde-format
msgid "Applications"
msgstr ""

#: ui/main.qml:173
#, kde-format
msgid "Login Scripts"
msgstr ""

#: ui/main.qml:176
#, fuzzy, kde-format
#| msgid "Pre-KDE startup"
msgid "Pre-startup Scripts"
msgstr "Εκκίνηση προ KDE"

#: ui/main.qml:179
#, fuzzy, kde-format
#| msgid "Logout"
msgid "Logout Scripts"
msgstr "Αποσύνδεση"

#: ui/main.qml:187
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: ui/main.qml:188
#, kde-kuit-format
msgctxt "@info 'some' refers to autostart items"
msgid "Click the <interface>Add…</interface> button to add some"
msgstr ""

#: ui/main.qml:203
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Choose Login Script"
msgstr "Προσθήκη σεναρίου..."

#: ui/main.qml:223
#, kde-format
msgid "Choose Logout Script"
msgstr ""

#: unit.cpp:26
#, kde-format
msgctxt "@label Entry is running right now"
msgid "Running"
msgstr ""

#: unit.cpp:27
#, kde-format
msgctxt "@label Entry is not running right now (exited without error)"
msgid "Not running"
msgstr ""

#: unit.cpp:28
#, fuzzy, kde-format
#| msgid "Startup"
msgctxt "@label Entry is being started"
msgid "Starting"
msgstr "Έναρξη"

#: unit.cpp:29
#, kde-format
msgctxt "@label Entry is being stopped"
msgid "Stopping"
msgstr ""

#: unit.cpp:83
#, kde-format
msgid "Error occurred when receiving reply of GetAll call %1"
msgstr ""

#: unit.cpp:153
#, kde-format
msgid "Failed to open journal"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Σπύρος Γεωργαράς, Τούσης Μανώλης"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sng@hellug.gr, manolis@koppermind.homelinux.org"

#, fuzzy
#~| msgid "KDE Autostart Manager Control Panel Module"
#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "Άρθρωμα πίνακα ελέγχου διαχειριστή εκκίνησης του KDE"

#, fuzzy
#~| msgid "Copyright © 2006–2010 Autostart Manager team"
#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "Copyright © 2006–2010 ομάδα διαχειριστή αυτόματης εκκίνησης"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "Συντηρητής"

#, fuzzy
#~| msgid "Advanced..."
#~ msgid "Add..."
#~ msgstr "Προχωρημένα..."

#~ msgid "Shell script path:"
#~ msgstr "Διαδρομή σεναρίου κελύφους:"

#~ msgid "Create as symlink"
#~ msgstr "Να δημιουργηθεί ως συμβολικός σύνδεσμος"

#, fuzzy
#~| msgid "Autostart only in KDE"
#~ msgid "Autostart only in Plasma"
#~ msgstr "Αυτόματη εκκίνηση μόνο στο KDE"

#~ msgid "Command"
#~ msgstr "Εντολή"

#, fuzzy
#~| msgctxt ""
#~| "@title:column The name of the column that decides if the program is run "
#~| "on kde startup, on kde shutdown, etc"
#~| msgid "Run On"
#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "Εκτέλεση σε"

#, fuzzy
#~| msgid "KDE Autostart Manager"
#~ msgid "Session Autostart Manager"
#~ msgstr "Διαχειριστής αυτόματης εκκίνησης του KDE"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "Ενεργό"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "Ανενεργό"

#~ msgid "Desktop File"
#~ msgstr "Αρχείο επιφάνειας εργασίας"

#~ msgid "Script File"
#~ msgstr "Αρχείο σεναρίου"

#~ msgid "Add Program..."
#~ msgstr "Προσθήκη προγράμματος..."

#~ msgid "Before session startup"
#~ msgstr "Πριν την εκκίνηση συνεδρίας"

#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr ""
#~ "Μόνο αρχεία με επέκταση « .sh » επιτρέπονται για τον ορισμό του "
#~ "περιβάλλοντος."

#~ msgid "Shutdown"
#~ msgstr "Τερματισμός"

#~ msgid "1"
#~ msgstr "1"

#~ msgid "Cancel"
#~ msgstr "Ακύρωση"

#~ msgid "Import"
#~ msgstr "Εισαγωγή"

#~ msgid "Add"
#~ msgstr "Προσθήκη"
